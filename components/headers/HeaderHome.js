import Avatar from "../Avatar";
import { ViewGridIcon } from "@heroicons/react/solid";
const url =
  "https://media-exp1.licdn.com/dms/image/D4E03AQGc7mqx6qh1Ig/profile-displayphoto-shrink_400_400/0/1642107338944?e=1647475200&v=beta&t=oUPNkDEISG8AqoD-CJQ2_qXu5y9-ypDYZrUDsk4ILlc";

function HeaderHome() {
  return (
    <header className="flex w-full p-5 justify-between text-sm text-gray-700">
      {/* Left */}
      <div className="flex space-x-4 items-center">
        <p className="custom-link">About</p>
        <p className="custom-link">Store</p>
      </div>
      {/* Right */}
      <div className="flex space-x-4 items-center">
        <p className="custom-link">Gmail</p>
        <p className="custom-link">Images</p>
        {/* Icon */}
        <ViewGridIcon className="h-10 w-10 p-2 rounded-full hover:bg-gray-100 cursor-pointer" />
        <Avatar url={url} />
      </div>
    </header>
  );
}

export default HeaderHome;
