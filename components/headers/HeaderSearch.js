import Image from "next/image";
import { useRouter } from "next/router";
import { useEffect, useRef } from "react";
import { MicrophoneIcon, SearchIcon, XIcon } from "@heroicons/react/outline";
import Avatar from "../Avatar";
import HeaderOptions from "./HeaderOptions";

const url =
  "https://media-exp1.licdn.com/dms/image/D4E03AQGc7mqx6qh1Ig/profile-displayphoto-shrink_400_400/0/1642107338944?e=1647475200&v=beta&t=oUPNkDEISG8AqoD-CJQ2_qXu5y9-ypDYZrUDsk4ILlc";

function HeaderSearch({ term, results }) {
  const router = useRouter();
  const searchInputRef = useRef(null);

  useEffect(() => {
    searchInputRef.current.value = term;
  }, []);

  const search = (e) => {
    e.preventDefault();
    const term = searchInputRef.current.value;

    if (!term) return;

    router.push(`/search?term=${term}`);
  };

  return (
    <header className="sticky top-0 z-50 bg-white">
      <div className="flex w-full p-6 items-center">
        <Image
          className="cursor-pointer"
          src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
          height={40}
          width={120}
          alt="Google"
          onClick={() => router.push("/")}
        />
        <form className="flex flex-grow border px-6 py-3 ml-10 mr-5 border-gray-200 rounded-full shadow-lg max-w-3xl items-center">
          <input
            className="flex-grow w-full focus:outline-none"
            type="text"
            ref={searchInputRef}
          />
          <XIcon className="h-7 sm:mr-3 text-gray-500 cursor-pointer transition duration-100 transform hover:scale-125 ease-out" />
          <MicrophoneIcon className="h-6 mr-3 hidden sm:inline-flex text-blue-500 border-l-2 pl-4 border-gray-300" />
          <SearchIcon className="h-6 text-blue-500 hidden sm:inline-flex" />
          <button hidden type="submit" onClick={search}>
            Search
          </button>
        </form>

        <Avatar className="ml-auto" url={url} />
      </div>

      <HeaderOptions />
    </header>
  );
}

export default HeaderSearch;
