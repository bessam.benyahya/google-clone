import HeaderOption from "./Header";
import {
  DotsVerticalIcon,
  MapIcon,
  NewspaperIcon,
  PhotographIcon,
  PlayIcon,
  SearchIcon,
} from "@heroicons/react/outline";

function HeaderOptions() {
  return (
    <div className="flex w-full mx-auto px-3 border-b text-gray-700 text-sm sm:pl-[5%] md:pl-[14%] lg:pl-52 lg:text-base lg:justify-start lg:space-x-36">
      {/* Left */}
      <div className="flex space-x-6">
        <HeaderOption Icon={SearchIcon} title="All" selected />
        <HeaderOption Icon={PhotographIcon} title="Images" />
        <HeaderOption Icon={PlayIcon} title="Videos" />
        <HeaderOption Icon={NewspaperIcon} title="News" />
        <HeaderOption Icon={MapIcon} title="Maps" />
        <HeaderOption Icon={DotsVerticalIcon} title="More" />
      </div>
      {/* Right */}
      <div className="flex space-x-4">
        <p className="custom-link">Settings</p>
        <p className="custom-link">Tools</p>
      </div>
    </div>
  );
}

export default HeaderOptions;
