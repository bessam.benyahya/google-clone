function Avatar({ url, className }) {
  return (
    <img
      className={`rounded-full h-12 w-12 cursor-pointer transition duration-150 transform hover:scale-110 ease-out ${className}`}
      loading="lazy"
      src={url}
      alt="Avatar"
    />
  );
}

export default Avatar;
