import Head from "next/head";
import { useRouter } from "next/router";
import HeaderSearch from "../components/headers/HeaderSearch";
import SearchResults from "../components/SearchResults";
import Response from "../dummy-response";

function Search({ results }) {
  const router = useRouter();
  const { term } = router.query;

  return (
    <div className="">
      <Head>
        <title>{term} - Search Results</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HeaderSearch term={term} />

      <SearchResults results={results} />
    </div>
  );
}

export default Search;

export async function getServerSideProps(context) {
  const apiKey = process.env.API_KEY;
  const cx = process.env.CONTEXT_KEY;
  const { term, start } = context.query;

  const startIndex = start || "0";

  const data = await fetch(
    `https://www.googleapis.com/customsearch/v1?key=${apiKey}&cx=${cx}&q=${term}&start=${startIndex}`
  ).then((res) => res.json());

  return {
    props: {
      results: data,
    },
  };
}
