import Head from "next/head";
import Footer from "../components/Footer";
import HeaderHome from "../components/headers/HeaderHome";
import Main from "../components/Main";

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center h-screen">
      <Head>
        <title>Google Clone (for educational purposes)</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <HeaderHome />
      <Main />
      <Footer />
    </div>
  );
}
