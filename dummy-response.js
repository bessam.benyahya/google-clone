export default {
  kind: "customsearch#search",
  url: {
    type: "application/json",
    template:
      "https://www.googleapis.com/customsearch/v1?q={searchTerms}&num={count?}&start={startIndex?}&lr={language?}&safe={safe?}&cx={cx?}&sort={sort?}&filter={filter?}&gl={gl?}&cr={cr?}&googlehost={googleHost?}&c2coff={disableCnTwTranslation?}&hq={hq?}&hl={hl?}&siteSearch={siteSearch?}&siteSearchFilter={siteSearchFilter?}&exactTerms={exactTerms?}&excludeTerms={excludeTerms?}&linkSite={linkSite?}&orTerms={orTerms?}&relatedSite={relatedSite?}&dateRestrict={dateRestrict?}&lowRange={lowRange?}&highRange={highRange?}&searchType={searchType}&fileType={fileType?}&rights={rights?}&imgSize={imgSize?}&imgType={imgType?}&imgColorType={imgColorType?}&imgDominantColor={imgDominantColor?}&alt=json",
  },
  queries: {
    request: [
      {
        title: "Google Custom Search - Paris",
        totalResults: "38800000",
        searchTerms: "Paris",
        count: 10,
        startIndex: 1,
        inputEncoding: "utf8",
        outputEncoding: "utf8",
        safe: "off",
        cx: "3cfe533f3e1b10575",
      },
    ],
    nextPage: [
      {
        title: "Google Custom Search - Paris",
        totalResults: "38800000",
        searchTerms: "Paris",
        count: 10,
        startIndex: 11,
        inputEncoding: "utf8",
        outputEncoding: "utf8",
        safe: "off",
        cx: "3cfe533f3e1b10575",
      },
    ],
  },
  context: {
    title: "Google",
  },
  searchInformation: {
    searchTime: 0.534741,
    formattedSearchTime: "0.53",
    totalResults: "38800000",
    formattedTotalResults: "38,800,000",
  },
  items: [
    {
      kind: "customsearch#result",
      title: "San Juan to Paris | Google Flights",
      htmlTitle: "San Juan to <b>Paris</b> | Google Flights",
      link: "https://www.google.com/travel/flights/search?tfs=CBwQAhoxagcIARIDU0pVEgoyMDIxLTEwLTA5cgwIAhIIL20vMDVxdGooAToCRjk6Ak5LkAGsAhoxagwIAhIIL20vMDVxdGoSCjIwMjEtMTAtMTlyBwgBEgNTSlUoAToCRjk6Ak5LkAGsAnABggELCP___________wFAAUgBmAEBagQQARgA&tfu=CnBDalJJZG01clRYaGtTVVU1V0VGQlJFcERXRkZDUnkwdExTMHRMUzB0TFhsemNHNHhOVUZCUVVGQlIwVnhUV1JOUkZGWmNrRkJFZ2N5T1RRNk16QTBHZ3NJNHRvQ0VBSWFBMVZUUkRnV2NPTGFBZz09&hl=en-US&curr=USD",
      displayLink: "www.google.com",
      snippet:
        "Find the best flights to Paris fast, track prices, and book with confidence.",
      htmlSnippet:
        "Find the best flights to <b>Paris</b> fast, track prices, and book with confidence.",
      cacheId: "xH4Odoxxbl8J",
      formattedUrl: "https://www.google.com/travel/flights/search?tfs...",
      htmlFormattedUrl: "https://www.google.com/travel/flights/search?tfs...",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from San Juan to Paris",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from San Juan to Paris",
            "og:description":
              "Departing Sat, Oct 9. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Sat, Oct 9. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/search?tfs=CBwQAhoxagcIARIDU0pVEgoyMDIxLTEwLTA5cgwIAhIIL20vMDVxdGooAToCRjk6Ak5LkAGsAhoxagwIAhIIL20vMDVxdGoSCjIwMjEtMTAtMTlyBwgBEgNTSlUoAToCRjk6Ak5LkAGsAnABggELCP___________wFAAUgBmAEBagQQARgA&tfu=CnBDalJJZG01clRYaGtTVVU1V0VGQlJFcERXRkZDUnkwdExTMHRMUzB0TFhsemNHNHhOVUZCUVVGQlIwVnhUV1JOUkZGWmNrRkJFZ2N5T1RRNk16QTBHZ3NJNHRvQ0VBSWFBMVZUUkRnV2NPTGFBZz09&hl=en-US&curr=USD",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Atlanta to Paris | Google Flights",
      htmlTitle: "Atlanta to <b>Paris</b> | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-atlanta-to-paris.html",
      displayLink: "www.google.com",
      snippet:
        "Find the best flights to Paris fast, track prices, and book with confidence.",
      htmlSnippet:
        "Find the best flights to <b>Paris</b> fast, track prices, and book with confidence.",
      cacheId: "Pk9l3oCT8YsJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-atlanta-to-paris.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-atlanta-to-<b>paris</b>.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Atlanta to Paris",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Atlanta to Paris",
            "og:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-atlanta-to-paris.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to Vienna | Google Flights",
      htmlTitle: "<b>Paris</b> to Vienna | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-paris-to-vienna.html",
      displayLink: "www.google.com",
      snippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You'll also get travel tips by email.",
      htmlSnippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You&#39;ll also get travel tips by email.",
      cacheId: "RDx0mQvI2pAJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-paris-to-vienna.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-<b>paris</b>-to-vienna.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUMJiI7ANvfPZb4tOkAuhFb3AzyA0uytRbhk1j85qdP3rZIlXpD3rWxYNwTjq-6R1sYE52h53-fxWmqA",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to Vienna",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to Vienna",
            "og:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-paris-to-vienna.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQUMJiI7ANvfPZb4tOkAuhFb3AzyA0uytRbhk1j85qdP3rZIlXpD3rWxYNwTjq-6R1sYE52h53-fxWmqA",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Boston to Paris | Google Flights",
      htmlTitle: "Boston to <b>Paris</b> | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-boston-to-paris.html",
      displayLink: "www.google.com",
      snippet:
        "Find the best flights to Paris fast, track prices, and book with confidence.",
      htmlSnippet:
        "Find the best flights to <b>Paris</b> fast, track prices, and book with confidence.",
      cacheId: "a7WG_Mbl-1AJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-boston-to-paris.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-boston-to-<b>paris</b>.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Boston to Paris",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Boston to Paris",
            "og:description":
              "Departing Wed, Jan 26. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Wed, Jan 26. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-boston-to-paris.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to Nairobi | Google Flights",
      htmlTitle: "<b>Paris</b> to Nairobi | Google Flights",
      link: "https://www.google.com/flights/flights-from-paris-to-nairobi.html",
      displayLink: "www.google.com",
      snippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You'll also get travel tips by email.",
      htmlSnippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You&#39;ll also get travel tips by email.",
      cacheId: "6CbNEwLgItUJ",
      formattedUrl:
        "https://www.google.com/flights/flights-from-paris-to-nairobi.html",
      htmlFormattedUrl:
        "https://www.google.com/flights/flights-from-<b>paris</b>-to-nairobi.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJgZqWThMIEJU8EWQEL8hcZzOxK1ji3sApa3zoxjauXUyMyo3Z-u8UadAGGFTbn-dBXo--CTTj0KZ_wA",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to Nairobi",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to Nairobi",
            "og:description":
              "Departing Wed, Jan 26. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Wed, Jan 26. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/flights/flights-from-paris-to-nairobi.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJgZqWThMIEJU8EWQEL8hcZzOxK1ji3sApa3zoxjauXUyMyo3Z-u8UadAGGFTbn-dBXo--CTTj0KZ_wA",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to Chicago | Google Flights",
      htmlTitle: "<b>Paris</b> to Chicago | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-paris-to-chicago.html",
      displayLink: "www.google.com",
      snippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You'll also get travel tips by email.",
      htmlSnippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You&#39;ll also get travel tips by email.",
      cacheId: "qW5-Ky3dwBkJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-paris-to-chicago.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-<b>paris</b>-to-chicago.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTDuw95ynLqGCkZRzt9XuWXGBs8qWhw_1UZkMrCRAQp3rojEZBpwitHM8GAMBXgKhSKFgJZcXZ3qmYs2g",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to Chicago",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to Chicago",
            "og:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Thu, Jan 27. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-paris-to-chicago.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcTDuw95ynLqGCkZRzt9XuWXGBs8qWhw_1UZkMrCRAQp3rojEZBpwitHM8GAMBXgKhSKFgJZcXZ3qmYs2g",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to New York | Google Flights",
      htmlTitle: "<b>Paris</b> to New York | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-paris-to-new-york.html",
      displayLink: "www.google.com",
      snippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You'll also get travel tips by email.",
      htmlSnippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You&#39;ll also get travel tips by email.",
      cacheId: "j_uHTGI0Md4J",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-paris-to-new-york.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-<b>paris</b>-to-new-york.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRa1dwH1NZQk4dsbPiyA9Vrq2RX75jXSZ-OkHn7NkLHl-11HercqDXoZ4p2cDxJkH6vca2degawrCzHEQ",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to New York",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to New York",
            "og:description":
              "Departing Fri, Jan 28. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Fri, Jan 28. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-paris-to-new-york.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRa1dwH1NZQk4dsbPiyA9Vrq2RX75jXSZ-OkHn7NkLHl-11HercqDXoZ4p2cDxJkH6vca2degawrCzHEQ",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to New York | Google Flights",
      htmlTitle: "<b>Paris</b> to New York | Google Flights",
      link: "https://www.google.com/flights/flights-from-paris-to-new-york.html",
      displayLink: "www.google.com",
      snippet:
        "Find the best flights to New York fast, track prices, and book with confidence.",
      htmlSnippet:
        "Find the best flights to New York fast, track prices, and book with confidence.",
      cacheId: "2ynq4MTrBNQJ",
      formattedUrl:
        "https://www.google.com/flights/flights-from-paris-to-new-york.html",
      htmlFormattedUrl:
        "https://www.google.com/flights/flights-from-<b>paris</b>-to-new-york.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRa1dwH1NZQk4dsbPiyA9Vrq2RX75jXSZ-OkHn7NkLHl-11HercqDXoZ4p2cDxJkH6vca2degawrCzHEQ",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to New York",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to New York",
            "og:description":
              "Departing Fri, Jan 28. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Fri, Jan 28. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/flights/flights-from-paris-to-new-york.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcRa1dwH1NZQk4dsbPiyA9Vrq2RX75jXSZ-OkHn7NkLHl-11HercqDXoZ4p2cDxJkH6vca2degawrCzHEQ",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Miami to Paris | Google Flights",
      htmlTitle: "Miami to <b>Paris</b> | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-miami-to-paris.html",
      displayLink: "www.google.com",
      snippet:
        "Find the best flights to Paris fast, track prices, and book with confidence.",
      htmlSnippet:
        "Find the best flights to <b>Paris</b> fast, track prices, and book with confidence.",
      cacheId: "HF-YTarkKgIJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-miami-to-paris.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-miami-to-<b>paris</b>.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Miami to Paris",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Miami to Paris",
            "og:description":
              "Departing Sat, Jan 29. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Sat, Jan 29. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-miami-to-paris.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQGx8ii2KbSDdbdzfKye5oDN2bwBA6audqI7XUEf2iMRZezpn_ZbQe1ZIuvUSH-8XOMe958umDwSsAF1w",
          },
        ],
      },
    },
    {
      kind: "customsearch#result",
      title: "Paris to Berlin | Google Flights",
      htmlTitle: "<b>Paris</b> to Berlin | Google Flights",
      link: "https://www.google.com/travel/flights/flights-from-paris-to-berlin.html",
      displayLink: "www.google.com",
      snippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You'll also get travel tips by email.",
      htmlSnippet:
        "Best departing flights ... Get email updates when prices change for your searched dates. You&#39;ll also get travel tips by email.",
      cacheId: "CzfZxkaERRUJ",
      formattedUrl:
        "https://www.google.com/travel/flights/flights-from-paris-to-berlin.html",
      htmlFormattedUrl:
        "https://www.google.com/travel/flights/flights-from-<b>paris</b>-to-berlin.html",
      pagemap: {
        metatags: [
          {
            "application-name": "Travel",
            "og:image":
              "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQt2sQ7Aqn9XDqztmoYElQwE26oMklrcGD7-ZuKOUPZVDpWlL-yn0uHraRpfjA9kXtiLrBxI-YW45Rn4g",
            "og:type": "website",
            "twitter:card": "summary",
            "twitter:title": "✈ Trip from Paris to Berlin",
            "og:site_name": "Google Flights",
            "apple-mobile-web-app-title": "Travel",
            "og:title": "✈ Trip from Paris to Berlin",
            "og:description":
              "Departing Sat, Jan 29. Find the best flights fast, track prices, and book with confidence.",
            "twitter:image":
              "https://www.google.com/images/branding/googleg/2x/googleg_standard_color_96dp.png",
            referrer: "origin",
            "apple-mobile-web-app-status-bar-style": "black",
            "msapplication-tap-highlight": "no",
            viewport:
              "width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no",
            "apple-mobile-web-app-capable": "yes",
            "twitter:description":
              "Departing Sat, Jan 29. Find the best flights fast, track prices, and book with confidence.",
            "mobile-web-app-capable": "yes",
            "og:url":
              "https://www.google.com/travel/flights/flights-from-paris-to-berlin.html",
          },
        ],
        cse_image: [
          {
            src: "https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQt2sQ7Aqn9XDqztmoYElQwE26oMklrcGD7-ZuKOUPZVDpWlL-yn0uHraRpfjA9kXtiLrBxI-YW45Rn4g",
          },
        ],
      },
    },
  ],
};
